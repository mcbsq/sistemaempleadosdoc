---
sidebar_position: 1
---

# Intro

Bienvenido al **Manual de Usuario** del **Sistema de Empleados - Cibercom**.

Este documento etá diseñado para proporcionarte una guía completa sobre el uso eficiente y efectivo de nuestra plataforma, destinada a simplificar y optimizar la gestión de empleados de **Cibercom**.

A lo largo de este manual explorarás las diversas funcionalidades y características del sistema.

Agradecemos tu compromiso con la excelencia y la mejora continua. Esperamos que esta guía te sea útil y que el Sistema de Empleados contribuya significativamente al éxito de la empresa.

