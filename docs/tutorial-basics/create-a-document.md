---
sidebar_position: 1
---

# Pantalla | Home

Esta, es la pantalla principal del **Sistema de Empleados** de **Cibercom**.

En ella, podrás encontrar un Carrusel de fotos, en donde cada tarjeta equivale a un empleado de la empresa, a su vez, cada foto contien un botón llamado **Ver** que te redirige al perfil de cada uno de ellos.

![Imagen de Muestra](./img/Carrusel.jpg)

## Navegación

el Carrusel contiene en la parte inferir del módulo dos botones: **Izquierda** y **Derecha**, los cuales servirán para navegar através de las tarjetas que se muestran. 