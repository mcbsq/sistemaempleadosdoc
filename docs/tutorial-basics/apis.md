---
sidebar_position: 5
---

# Apis

## Home 
En esta pantalla la cual utuliza useState para gestionar dos estados locales:
~~~
const [empleados, setEmpleados] = useState([]);
const [datosCargados, setDatosCargados] = useState(false);
~~~
- empleados: Un arreglo.
- datosCargados: Indica si los datos se han cargado.

Se muestran los empleados en forma de carrusel.
Se puede acceder a más información del usuario con el botón "Ver":
*Se usa la función cargarEmpleadosBD que utiliza fetch para realizar una solicitud a la URL http://localhost:3000/empleados y cargar datos de empleados.*

~~~
const cargarEmpleadosBD = () => {
  // ...
};
~~~

*Cuando se completa la solicitud, se actualiza el estado local con los datos de empleados y se marca datosCargados como true.*

. Una vez entrando al perfil del Empleado este puede modificar sus apellidos, fecha de nacimiento y su foto.


**Tambien podemos navegar entre los empleados con los botones next y preview**

~~~
<div className="button-container">
  <div className="button prev" onClick={handlePrevClick}></div>
  <div className="button next" onClick={handleNextClick}></div>
</div>
~~~


## Empleados

En esta pantalla que utiliza useState para gestionar varios estados locales, el empleado intriducirá sus datos:
 ~~~
const [imgActual, setImgActual] = useState([]);
const [modalTitulo, setModalTitulo] = useState("");
const [empleados, setEmpleados] = useState([]);
const [globalFilter, setglobalFilter] = useState("");
const [globalpage, setglobalpage] = useState(0);
const [datosCargados, setDatosCargados] = useState(false);
~~~
- modalTitulo: Controla el título del modal.
- empleados: Almacena la lista de empleados.
- globalFilter: Almacena el filtro global por nombres.
- globalpage: Almacena la página actual en la paginación.
- datosCargados: Indica si los datos de empleados han sido cargados.

**También utiliza useEffect para cargar empleados desde la base de datos**

~~~
useEffect(() => {
  cargarEmpleadosBD();
}, []);

const cargarEmpleadosBD = () => {
  // Se usa para cargar empleados desde la base de datos 
};
~~~

**Y actualiza el estado imgActual cuando cambian los archivos seleccionados.**
~~~
useEffect(() => {
  setImgActual(filesContent);
}, [filesContent]);
~~~


~~~
const editarEmpleado = () => 
~~~

La cual muestra sus datos básicos como nombre y fecha de nacimiento.




## Personal 
En esta pantalla que Utiliza useParams para extraer el parámetro id de la URL se muestra la información completa del Empleado organizada por apartados:
~~~
const { id } = useParams();
~~~
**También ulitiza useState para gestionar varios estados locales:**
- mpleados: Almacena la información de los empleados.
- datosCargados: Indica si los datos están cargados.
- isEditing: Indica si se está editando un empleado.
- isCreating: Indica si se está creando un nuevo empleado.
- EducacionCargada: Indica si la información de educación está cargada.
- redesSocialesCargadas: Indica si la información de redes sociales está cargada.
- expedienteClinicoCargado: Indica si el expediente clínico está cargado.

La información que proporcionada en esta pantalla es la siguiente:

- Intro: Proporciona una breve presentación del empleado.
**Utiliza useState que almcacenan la descripción del empleado**
~~~
const [descripcion, setDescripcion] = useState(
  "Por favor, edita tu descripción. En la parte inferior, encontrarás un botón que te permite editar todos los formularios. Para modificar cualquier sección, simplemente haz clic en el botón correspondiente y podrás actualizar la información según tus necesidades."
);
~~~

**Los apartados que se muestran son los siguientes:**

- Datos del contacto: Como su nombre lo indica son datos para contactar al empleado.

**Utiliza la función fetch para realizar una solicitud GET al servidor en http://localhost:3000/empleados**
~~~
const cargarEmpleadosBD = () => 
  fetch("http://localhost:3000/empleados", {
    method: "get",
    headers: {
      "Access-Control-Allow-Origin": "*",
    },
  })
~~~
**Cuando la respuesta se recibe, se convierte a formato JSON y se utiliza para actualizar el estado empleados mediante setEmpleados.**
**También se establece el estado datosCargados a true para indicar que los datos se han cargado correctamente.**
~~~
.then((response) => response.json())
    .then((json) => {
      setEmpleados(json);
      setDatosCargados(true); // Marcar que los datos se han cargado
    });
~~~

- Redes Sociales: Muestra las redes sociales del empleado 

**Los datos se cargan**
~~~
  const RedSocial = [
    { label: "Facebook", value: <CiFacebook /> },
    { label: "Instagram", value: <FaInstagram /> },
    { label: "Linkedin", value: <CiLinkedin /> },
    { label: "Youtube", value: <CiYoutube /> },
    { label: "tiktok", value: <FaTiktok /> },
  ];
~~~


- Expediente clinico: Contiene la información médica del empleado.

**Utiliza useState para gestionar el estado local  selectedFiles. Este estado almacena la información de los archivos seleccionados.**
~~~
const [selectedFiles, setSelectedFiles] = useState([]);
~~~

**El empleado puede subir un archivo pdf proporcionando su informacón médica**
~~~
const { openFilePicker, filesContent } = useFilePicker({
  readAs: "DataURL",
  accept: "pdf/*",
  validators: [new FileAmountLimitValidator({ max: 1 })],
});
~~~

**También el empleado puede proporsionar su tipo de sangre**
~~~
const opcionesTipoSangre = ["A+", "A-", "B+", "B-", "AB+", "AB-", "O+", "O-"];
~~~

**Ya subidos los archivos el empleado puede guardar los cambios presionando en el botón "guardar"**
~~~
useEffect(() => {
  if (filesContent && filesContent.length > 0) {
    // Handle the selected files
    setSelectedFiles(filesContent);

    // Asigna el contenido del archivo a expedienteclinico.PDFSegurodegastosmedicos
    setexpedienteclinico((prev) => ({
      ...prev,
      PDFSegurodegastosmedicos: filesContent,
    }));
  }
}, [filesContent]);
~~~


- Educación: Muestra la escolaridad del empleado 
**Almacena una lista de elementos relacionados con la educación del empleado.**
~~~
const [educationItems, setEducationItems] = useState([
  {
    year: "2021 - 2022",
    title: "CIBERCOM",
    description: "Descripción de la experiencia",
  },
]);
~~~

**Crea un nuevo elemento de educación (newEducationItem) con valores predeterminados para el año, el título y la descripción.**
~~~
const handleAddEducation = () => {
  const newEducationItem = {
    year: "2021 - 2022",
    title: "Universidad",
    description: "Descripción de la educación",
  };
~~~

**Utiliza el operador de propagación (...) para crear una nueva lista que contiene todos los elementos actuales de educationItems y agrega el nuevo elemento al final de la lista.**
**Y también utiliza setEducationItems para actualizar el estado educationItems con la nueva lista que incluye el nuevo elemento.**
~~~
  setEducationItems([...educationItems, newEducationItem]);
~~~


- Experiencia: Muestra la experiencia laboral del empleado.
**Almacena una lista de elementos relacionados con la experiencia laboral del empleado, cada elemento tiene un año, un título y una descripción asociados a la experiencia laboral**
~~~
const [ExperienciaItems, SetExperienciaItems] = useState([
  {
    year: "2021 - 2022",
    title: "CIBERCOM",
    description: "Descripción de la experiencia",
  },
]);
~~~
**Crea un nuevo elemento de experiencia (newExperienceItem) con valores predeterminados para el año, el título y la descripción.**
~~~
const handleAddExperience = () => 
  const newExperienceItem = {
    year: "2021 - 2022",
    title: "Título de la experiencia",
    description: "Descripción de la experiencia",
  };
~~~  

**Utiliza el operador de propagación (...) para crear una nueva lista que contiene todos los elementos actuales de ExperienciaItems y agrega el nuevo elemento al final de la lista.**
**Y también utiliza SetExperienciaItems para actualizar el estado ExperienciaItems con la nueva lista que incluye el nuevo elemento.**
~~~
  SetExperienciaItems([...ExperienciaItems, newExperienceItem]);
~~~


- Habilidades profesionales: Muestra el conocimiento del empleado.
**Almacena una lista de habilidades del empleado, cada habilidad tiene un nombre y un porcentaje asociado que indica el nivel de dominio de esa habilidad.**
~~~
const [habilidades, setHabilidades] = useState([
  { skillName: "Web Design", porcentaje: 0 },
]);
~~~

**Crea una nueva habilidad (nuevaHabilidad) con valores predeterminados para el nombre de la habilidad y un porcentaje inicial.**
~~~
const handleAddSkill = () => 
  const nuevaHabilidad = {
    skillName: "Nueva Habilidad",
    porcentaje: 0,
  };
~~~

***Utiliza el operador de propagación (...) para crear una nueva lista que contiene todas las habilidades actuales y agrega la nueva habilidad al final de la lista.**
**Y también utiliza setHabilidades para actualizar el estado habilidades con la nueva lista que incluye la nueva habilidad**
~~~
  setHabilidades([...habilidades, nuevaHabilidad]);
~~~


- Personas de contacto: Son las personas a las cuales contactaran en caso de un incidente.
**Se utiliza la función cargarPersonasContactoPorEmpleado para realizar una solicitud HTTP GET al servidor y obtener los datos de las personas de contacto asociadas a un empleado.**
~~~
const cargarPersonasContactoPorEmpleado = (empleadoId) => 
  fetch(`http://localhost:3000/personascontacto/empleado/${empleadoId}`, {
    method: "GET",
    headers: {
      "Access-Control-Allow-Origin": "*",
    },
  })
~~~


- Recursos humanos: Muestra información de los recursos humanos
Define un componente llamado renderRHSection utilizando una función de flecha. Este componente renderiza una sección que contiene etiquetas y campos de entrada condicionalmente según el valor de la variable isEditing.

**El componente devuelve una estructura de elementos JSX que representa una sección visual en la interfaz de usuario.**
~~~
<div className="RH-column">
  <div className="RH-box">
    <div className="RH-content">
      <div className="content">
        {/* Contenido aquí */}
      </div>
    </div>
  </div>
</div>
~~~

**Utiliza la expresión ternaria isEditing ? (...) : (...) para decidir si mostrar un campo de entrada o un párrafo vacío para cada etiqueta. Si isEditing es true, se renderiza un campo de entrada y si no se renderiza un párrafo vacío.**
~~~
<label>Puesto</label>
{isEditing ? (
  <input
    type="text"
  />
) : (
  <p></p>
)}

<label>Jefe Inmediato</label>
{isEditing ? (
  <input
    type="text"
  />
) : (
  <p></p>
)}

<label>Horario Laboral</label>
{isEditing ? (
  <input
    type="text"
  />
) : (
  <p></p>
)}

<label>Zona Horaria</label>
{isEditing ? (
  <input
    type="text"
  />
) : (
  <p></p>
)}
~~~



## Login

En esta pantalla se muestra el login, donde se utilizan los useState para gestionar varios estados locales:
~~~
const [email, setEmail] = useState("");
const [password, setPassword] = useState("");
const [showPassword, setShowPassword] = useState(false);
const [isRegisterModalOpen, setRegisterModalOpen] = useState(false);
~~~
 - email: Correo del usuario.
 - password: Contraseña del usuario.
 - isRegisterModalOpen: Muestra u oculta la contraseña en el formulario.
 - showPassword: Controla la visibilidad de un modal de registro.


En la cual el empleado debe llenar con los datos sus datos como correo y contraseña. Si es usuario nuevo debe registrarse y si ya está registrado solo debe iniciar sesión.

**Ingresamos el email y la contraseña:**
~~~
const handleEmailChange = (e) => {
  setEmail(e.target.value);
};

const handlePasswordChange = (e) => {
  setPassword(e.target.value);
};
~~~

Una vez creado el perfil de usuario este puede modificarse a gusto propio para mostrar datos del empleado así como su formación académica y laboral. 


