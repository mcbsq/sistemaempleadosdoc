---
sidebar_position: 3
---

# Pantalla | Personal

Esta pantalla compila toda aquella información personal y profesional del empleado, lo que conlleva a mostrar un perfil completo del mismo, por lo que el administrador del sistema tendrá la facilidad de navegar por cada área y conocer los avances de los proyectos vigentes.

## Descripción del Empleado

Esta parte del perfil es una oportunidad para que los empleados añadan una dimensión personal a su presencia profesional en la empresa. Pueden escribir sobre sus intereses, experiencia laboral, y cualquier otra información que pueda ser relevante para el equipo de recursos humanos o sus compañeros de trabajo.

- **Edición de Descripción**: Un botón de "Editar" está disponible para hacer cambios. Al hacer clic en este botón, se habilita un campo de texto donde se puede introducir o modificar la descripción. Esta característica facilita mantener un perfil actualizado y personalizado.

![Imagen de Muestra](./img/Intro.jpg)

## Datos de Contacto

Los datos de contacto son esenciales para la comunicación efectiva dentro de la empresa. Esta sección está diseñada para capturar todos los canales de comunicación relevantes del empleado, asegurando que la empresa pueda contactarlos por varios medios en diferentes situaciones.

- **Teléfono Fijo**: Para contactos de la oficina o locales.
- **Teléfono Celular (Obligatorio)**: Un medio directo y personal para comunicaciones urgentes.
- **Dirección**: Información vital para la correspondencia o en caso de necesidad de visitas oficiales.
- **ID de WhatsApp/Telegram**: Para mensajería instantánea y comunicaciones rápidas.
- **Correo electrónico**: Un canal oficial para comunicaciones formales y documentación.

Los campos están pre-configurados para aceptar sólo los formatos de entrada pertinentes, asegurando la validez de la información proporcionada.

![Imagen de Muestra](./img/DatosContacto.jpg)

## Personas de Contacto

La sección de Personas de Contacto es crítica para situaciones de emergencia o para obtener referencias. Permite a los empleados listar individuos externos que pueden ser contactados en caso de que el empleado no esté disponible.

- **Nombre de Contacto y Parentesco**: Identificación de la persona y su relación con el empleado.
- **Número de Teléfono y Correo del Contacto**: Diferentes métodos para alcanzar a la persona de contacto rápidamente.
- **Dirección del Contacto**: Información útil si se requiere un contacto en persona.

Los empleados pueden añadir múltiples contactos, proporcionando así una red de seguridad robusta para circunstancias imprevistas. Los datos pueden ser revisados y modificados utilizando un interfaz intuitivo y fácil de usar.

![Imagen de Muestra](./img/PersonasContacto.jpg)

Cada uno de estos apartados está diseñado para garantizar que los perfiles de los empleados sean completos, precisos y útiles tanto para la empresa como para el propio empleado. La interacción con estos campos se hace sencilla y directa, gracias a una interfaz amigable y botones claramente marcados para la edición y actualización de la información.
 
# Redes Sociales 
 
En esta sección, encontrarás información detallada sobre las redes sociales en nuestro proyecto. 
se manejan las siguientes variables relacionadas con nuestras redes sociales: 
 
- Red Social Seleccionada 
- URL Red Social 
- Nombre Red Social 

![Imagen de Muestra](./img/RedesSociales.jpg)
 
### Edición de Variables 
 
En esta sección, puedes realizar ediciones para ajustar la información de las redes sociales según las necesidades del proyecto. 
 
# Expediente clinico 
 
En esta sección, encontrarás información detallada sobre el expediente clínico en nuestro proyecto. 
En este bloque, se manejan las siguientes variables relacionadas con el expediente clínico. 
 
- Tipo de sangre 
- Padecimientos 
- Numero Seguro Social 
- Segurode Gastos Medicos 
- PDF Segurode Gastos Medicos 

![Imagen de Muestra](./img/ExpedienteClinico.jpg)
 
### Edición de Variables 
 
En esta sección, puedes realizar ediciones para ajustar la información del expediente clínico según las necesidades del proyecto.

## Trayectoria

En este apartado se muestran dos ventanas en conjunto en las que se describe la experiencia que ha tenido el empleado y los estudios con los que valida su conocimiento, teniendo en la izquiera **Educación** y a la derecha **Experiencia**.

![Imagen de Muestra](./img/Trayectoria.jpg)

### Educación

En este módulo del sistema vamos a encontrar el intervalo de años en los que se obtuvo un estudio, es decir, un grado profesional, diplomado, etc.

Posteriormente encontraremos el nombre de la institución que otorgó el grado y al final el grado obtenido.

### Experiencia

Por otro lado, a la derecha encontramos el módulo de experiencia, basicamente tiene la misma información de **Educación** sólo que va enfocado a la experiencia laboral, teniendo como datos los años en los que se trabajó en determinada empresa, el nombre de la misma y el cargo que se desempeñó.

## Skills Profesionales | Habilidades Profesionales

Este módulo contiene una serie de barras de progreso que muestran aproximadamente el porcentaje de conocimiento que el empleado considera tener en determinada habilidad, siendo el caso de los desarrolladores el nivel de conocimiento de los lenguajes que utiliza.

![Imagen de Muestra](./img/Habilidades.jpg)

El Empleado va a tener la posibilidad de arrastrar esa barra hasta el porcentaje que requiera, posteriormente este valor numérico se guardará en la base de datos.

## Recursos humanos 
 
En esta sección, encontrarás información detallada sobre los recursos humanos en nuestro proyecto. 
Dentro de este bloque, se manejan las siguientes variables: 
 
- Puesto  
- Jefe Inmediato 
 
 Horario laboral 
 
- Hora de Entrada 
- Hora de Salida 
- Tiempo de Comida 
- Días Trabajados 

![Imagen de Muestra](./img/RecursosHumanos.jpg)
 
### Edición de Variables 
 
Dentro de este bloque, puedes realizar las ediciones necesarias para ajustar las variables de recursos humanos según las necesidades del proyecto. 

