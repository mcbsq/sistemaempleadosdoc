---
sidebar_position: 4
---

# Pantalla | Login

La pantalla de login es la puerta de entrada al sistema de CIBERCOM. En ella, se solicita al usuario su nombre de usuario y contraseña para poder acceder, siempre y cuando ya se cuente con un registro previo.

![Imagen de Muestra](./img/Login.jpg)

### Iniciar Sesión

Para ingresar al sistema, el usuario debe proporcionar sus credenciales válidas:

  - Usuario
  - Contraseña

Una vez introducidos los datos, debe pulsar el botón "Iniciar sesión" para acceder.



Si el usuario no está registrado, puede hacerlo a través del botón "Registrarse".

### Registro de Usuario

Al seleccionar "Registrarse", se redirige al usuario a una pantalla donde puede crear una nueva cuenta:

  - Usuario
  - Contraseña

Después de completar la información, el usuario debe guardar la información para crear su cuenta.


# Pantalla | Registro

Si un usuario nuevo desea acceder al sistema, debe registrarse mediante la pantalla de registro, la cual solicita información básica para la creación de una cuenta.

![Imagen de Muestra](./img/AgregarRegistro.jpg)

### Crear Nuevo Usuario

El formulario de registro solicita los siguientes datos:

  - Nombre de Usuario
  - Contraseña

Tras rellenar estos campos, el usuario puede guardar la información con el botón "Guardar" o cancelar el proceso con el botón "Cancelar".

Una vez registrado, el usuario puede volver a la pantalla de login para ingresar al sistema con sus nuevas credenciales.

![Imagen de Muestra](./img/LoginPrueba.jpg)

# Pantalla | Inicio

Tras un inicio de sesión exitoso, los usuarios son llevados a la pantalla principal o "Home" del sistema de CIBERCOM. Esta pantalla sirve como un dashboard que proporciona acceso rápido a varias funciones del sistema.

### Bienvenida

Al entrar, el usuario es recibido con una interfaz amigable que muestra información relevante y accesos directos a las funcionalidades más importantes del sistema.

![Imagen de Muestra](./img/LoginUsuarios.jpg)

### Accesos Directos

En la pantalla de inicio, los usuarios pueden encontrar iconos o botones que funcionan como accesos directos a diferentes secciones, como:

- Organigrama de la empresa
- Gestión de empleados
- Configuraciones del sistema

Cada uno de estos accesos incluye una breve descripción y un botón "Ver" que redirige al usuario a la pantalla correspondiente.

### Navegación

La navegación es intuitiva, con una barra de navegación en la parte superior que permite al usuario moverse fácilmente a través de las diferentes secciones como Organigrama, Empleados y su perfil de usuario.

En la parte inferior, se encuentran controles de navegación que permiten desplazarse por diferentes páginas o secciones del dashboard si están disponibles.

