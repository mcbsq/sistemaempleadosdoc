---
sidebar_position: 1
---

# Pantalla | Empleados

En esta pantalla en especial, vamos a encontar una tabla donde se encuentran funciones **CRUDS** con la finalidad de gestionar a los empleados de la empresa **CIBERCOM**.

## Tabla de Empleados

Este elemento de la pantalla es el más importante, ya que aqui suceden los procesos CRUDS, siendo estos **GET, POST, PUT Y DELETE**.

![Imagen de Muestra](./img/TablaEmpleados1.jpg)

### Función GET

A través de la Api-Rest, el sistema obtiene la información de la base de datos con la función **"GET"**, lo que muestra los encabezados de las columnas y filas que se encuentran en la base de datos, así como la información que estas contienen.

![Imagen de Muestra](./img/TablaEmpleados.jpg)

### Agregar Empleado

Empezamos con la función inicial de la tabla (**POST**).

Cuando le damos click, al botón **"Agregar Empleado"** nos genera una pantalla flotante (Pop-Up) con un formulario en el que se debe agregar la información personanl de Empleado, siendo esta:

  - Nombre
  - Apellido Paterno
  - Apellido Materno
  - Fecha de Nacimiento
  - Fotografía del Empleado

  ![Imagen de Muestra](./img/AgregarEmpleado.jpg)

  Posteriormente al guardar la información, esta se guarda en la Base de Datos haciendo un POST mediante la Api Rest.
  
### Editar Empleado

Esta función está totalmente relacionada con la anterior, ya que edita/actualiza la información anteriormente agregada.

![Imagen de Muestra](./img/EditarEmpleado.png)

Este botón **"Editar"** se muestra a un lado de cada usuario ingresado a la tabla, posteriormente el sistema manda a llamar la misma pantalla flotante de la función anterior con la información pre-cargada lista para ser actualizada.

![Imagen de Muestra](./img/EditarEmpleado1.jpg)

### Eliminar Empleado

En la tabla **"Empleados"** al lado de cada usuario agregado, se mostrará un botón llamado **"Eliminar"** lo que permite que el registro de la información anteriormente cargada en la base de datos pueda eliminarse através de la Api-Rest.

![Imagen de Muestra](./img/EliminarEmpleado.png)

Al seleccionar el botón **"Eliminar"** el navegador muestra una pantalla de confirmación, donde valida que el usuario ha sido eliminado satisfactoriamente.

![Imagen de Muestra](./img/EliminarEmpleado1.jpg)

### Filtrado | Busqueda de Registros

En la esquina superior izquierda de la Tabla de Empleados, vamos a encontrar un **Campo de Texto** que nos permitirá buscar a un empleado en específico de acuerdo al nombre del mismo, lo que genera un filtro de busqueda interno.

![Imagen de Muestra](./img/BusquedaEmpleados.jpg)



### Paginación de Registros

En la esquina inferior izquierda de la Tabla de Empleados, vamos a encontrar dos botones de navegación que nos permitirá cambiar de página dentro de los registros de la misma.

Esto debido a que la Tabla de Empleados obtiene 5 registros por página.

Estos botones nos permiten navegar a la página **Siguiente** y a la página **Anterior**